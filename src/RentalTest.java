import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RentalTest {
    Movie testmMovie = new Movie("StarWars", 0);
    Movie testmMovie2 = new Movie("StarWars", 1);
    Movie testmMovie3 = new Movie("StarWars", 2);

    Rental testRental = new Rental(testmMovie, 4);
    Rental testRental2 = new Rental(testmMovie2, 3);
    Rental testRental3 = new Rental(testmMovie3, 5);
    @Test
    void getDaysRented() {
        assertEquals(4, testRental.getDaysRented());
    }

    @Test
    void getMovie() {
        assertEquals(testmMovie, testRental.getMovie());
    }
    @Test
    void getCharge(){
        assertEquals(5, testRental.getCharge());
    }
    @Test
    void getFrequentRenterPoints(){
        assertEquals(1, testRental.getFrequentRenterPoints());
        assertEquals(2, testRental2.getFrequentRenterPoints());
        assertEquals(1, testRental3.getFrequentRenterPoints());
    }
}