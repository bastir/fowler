import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovieTest {

    Movie testmovie = new Movie("Star Wars", 1);
    @Test
    void getPriceCode() {
        assertEquals(1, testmovie.getPriceCode());
    }

    @Test
    void setPriceCode() {
        testmovie.setPriceCode(2);
        assertEquals(2, testmovie.getPriceCode());
    }

    @Test
    void getTitle() {
        assertEquals("Star Wars", testmovie.getTitle());
    }
}