import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    Customer test1 = new Customer("Steve");
    Movie testmMovie = new Movie("StarWars", 0);

    Rental testRental = new Rental(testmMovie, 4);


    @org.junit.jupiter.api.Test
    void getName() {
        assertEquals("Steve", test1.getName());
    }

    @org.junit.jupiter.api.Test
    void statement() {
        test1.addRental(testRental);
        String  teststring= "Rental Record for " + "Steve" + "\n";
        teststring += "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";
        teststring += "\t" + "StarWars"+ "\t" + "\t" + "4" + "\t" + "5.0" + "\n";
        teststring += "Amount owed is " + "5.0" + "\n";
        teststring += "You earned " + "1" + " frequent renter points";
        assertEquals(teststring, test1.statement());

    }
}